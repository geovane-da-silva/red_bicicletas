var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var body = require('body-parser');
var passport = require('./config/passport');
//var session = require('express-session');
var session = require('session');
var jwt = require('jsonwebtoken');

var indexRouter = require('./routes/index');
var usuariosRouter = require('./routes/usuarios');
var tokenRouter = require('./routes/token');

var bicisRouter = require("./routes/bicicletas");
var bicicletaAPIRouter = require('./routes/api/bicicletas');
var usuarioAPIRouter = require('./routes/api/usuarios');
var authRouter = require('./routes/api/auth');

var Usuario = require('./models/usuario');
var Token = require('./models/token');

//var store = new session.MemoryStore;
/*
app.use(session({
  cookie: { maxAge: 240 * 60 * 60 * 100 },
  store: store,
  saveUninitialized: true,
  resave: true,
  secret: "metamorf//"
}));
*/

var app = express();
app.set('secretKey', 'jwt_pwd12345');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
//app.use(express.session({ secret: "keyboard cat"}));
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

//autenticacion
app.get("/login", function(req, res){
  res.render("session/login");
});

app.post("/login", function(req, res, next){

  passport.authenticate('local', function(err, usuario, info){
    if(!usuario) return res.render('session/login', {info});
    req.user = usuario;
    console.log('req.user....' + req.user);
    req.logIn(usuario, function(err){
      if(err) return next(err);
      return res.redirect('/');
    });
  })(req, res, next);

});


app.get("/logout", function(req, res){
  req.logout();
  res.render("session/login");
});

app.get("/forgotPassword", function(req, res){
  res.render('session/forgotPassword');
});

app.get("/forgotPasswordMessage", function(req, res){
  res.render('session/forgotPasswordMessage');
});

app.post("/forgotPassword", function(req, res, next){
  Usuario.findOne({email: req.body.email}, function(err, usuario){
    if(!usuario) return res.render('session/forgotPassword', {info: { message: "No existe email, para un usuario existente"}});
    usuario.resetPassword(function(err){
      if(err) next(err);
      console.log('session/forgotPasswordMessage');
    });
    res.render('session/forgotPasswordMessage');
  });
});


app.get("/resetPassword/token/:token", function(req, res, next){
  console.log(req.params.token);
  Token.findOne({ token: req.params.token }, function(err, token){
    if(err) return res.status(500).send(err);
    if(!token) return res.status(400).send( { type: 'not-verified', message: 'No existe un usuario asociado al token.  Verifique que su token no haya expuirado.'});
    Usuario.findById(token._userId, function(err, usuario){
      if(!usuario) return res.status(400).send({ message: "No existe un usuario asociado al token"});
      res.render('session/resetPassword', { errors: {}, usuario: usuario} );
    });
  });
});


app.post("/resetPassword", function(req, res){
  if(req.body.password != req.body.confirm_password)
    res.render('session/resetPassword', { errors: {confirm_password: "No coincide el pawwsord ingresado."}, usuario: new Usuario({email: req.body.email}) });
    Usuario.findOne({email: req.body.email}, function(err, usuario){
      usuario.password = req.body.password;
      console.log('cambio de password a: ' + req.body.email);
      usuario.save(function(err){
        if(err) return res.render('session/resetPassword', {errors: err.errors, usuario: new Usuario({email: req.body.email })});
        res.redirect('/login')
      });
    });
});

app.use('/', indexRouter);
app.use('/usuarios', usuariosRouter);
app.use('/token', tokenRouter);
app.use('/bicicletas', loogedIn, bicisRouter);

app.use('/api/auth', authRouter);
app.use('/api/bicicletas', bicicletaAPIRouter);
app.use('/api/usuarios', usuarioAPIRouter);


//var dev_db_url = 'mongodb://root:admin1234@ds157516.mlab.com:57516/red_bicicletas';
//var mongoDB = process.env.MONGODB_URI || dev_db_url;

var mongoose = require('mongoose');
var mongoDB = "mongodb://localhost/red_bicicletas"
//var mongoDB = "mongodb://root:admin1234@ds157516.mlab.com:57516/red_bicicletas"; //mlab
//var mongoDB = "mongodb+srv://root:admin1234@cluster0-dvync.mongodb.net/red_bicicletas?retryWrites=true"; //mongo atlas

mongoose.connect(mongoDB, {useNewUrlParser: true});
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

function loogedIn(req, res, next){
  if(req.user) next();
  else{
    console.log('user sin loguerse')
    res.redirect('/login');
  }
}

function validarUsuario(req, res, next){
  jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function(err, decoded){
    if(err)
      res.json({status: 'error', message: err.message, data: null});
    else{

      req.body.userId = decoded.id;
      console.log('jwt veyfied ' + decoded);
      next();

    }
  });
}

module.exports = app;
