var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var biciletaSchema = new Schema({
    code: Number,
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number], index: {type: "2dsphere", sparse: true}
    }
});


biciletaSchema.statics.createInstance = function(code, color, modelo, ubicacion){
    return new this ({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    });
}

biciletaSchema.methods.toString = function(){
    return "code: " + this.code + " color: " + this.color; 
}

var Bicicleta = mongoose.model('Bicicleta', biciletaSchema);

exports.add = function(req, res) {

    const bike = new Bicicleta(req);
    bike.save(err => {
        if (err) return res.status(500).send(err);
        res.status(200).send(bike);
    });
}

exports.allBicis = function(req, res){
    Bicicleta.find({}, function(err, bikes){
        if (err) return res.status(500).send(err);
        return res.status(200).send(bikes);
    })
}

exports.findById = function(id, res){

    Bicicleta.findById({_id: id}, function (err, bike) { 
        if (err) return res.status(500).send(err);
        return res.status(200).send(bike);
    });
}

exports.UpdateById = function(bici, res){
    
    Bicicleta.findByIdAndUpdate(bici._id, {$set:bici}, function(err, bike) {
        if (err) return res.status(500).send(err);
        return res.status(200).send(bike);
    });
}

exports.RemoveById = function(id, res){

    Bicicleta.findByIdAndRemove({_id: id}, function(err, bike){
        if (err) return res.status(500).send(err);
        return res.status(200).send(bike);
    });
}

//module.exports = Bicicleta;

/*
var Bicicleta = function(id, color, modelo, ubicacion){
    this.id = id,
    this.color = color,
    this.modelo = modelo,
    this.ubicacion = ubicacion

}

Bicicleta.prototype.toString = function(){
        return 'id: ' + this.id + " | color: " + this.color;
}


Bicicleta.allBicis = [];
Bicicleta.add = function(aBici){
    Bicicleta.allBicis.push(aBici);
}


var a = new Bicicleta(1,'Azul', 'Urbano', [14.6070454,-90.5145761]);
var b = new Bicicleta(2,'Rojo', 'Urbano', [14.6539966,-90.5745952]);

Bicicleta.add(a);
Bicicleta.add(b);


Bicicleta.findById = function(id){
      var aBici = Bicicleta.allBicis.find(x=>x.id==id)
      
      if(aBici)
        return aBici
      
      throw new Error(`No existe una bicicleta con el id ${id}`)  
}


Bicicleta.RemoveById = function(id){
    for(var i = 0; i < Bicicleta.allBicis.length;i++){
        if(Bicicleta.allBicis[i].id == id){
            Bicicleta.allBicis.splice(i,1);
            break
        }   

    }
}

Bicicleta.UpdateById = function(bici){
    for(var i = 0; i < Bicicleta.allBicis.length;i++){

        var dto = Bicicleta.allBicis[i].id;

        if(dto.id == bici.id){
            dto.color = bici.color;
            dto.modelo = bici.modelo;
            dto.ubicacion = bici.ubicacion;
            break
        }   
    }
}


module.exports = Bicicleta;
*/