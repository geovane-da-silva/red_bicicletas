var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var reserva = require('../models/reserva');
var Schema = mongoose.Schema;

//no soportado para windows
//const bcrypt = require('bcrypt');
//const saltRounds = 10;

var crypto = require('crypto');
var Token = require('../models/token');
var mailer = require('../mailer/mail');

/*
const validateEmail = function(email){
    const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3,4})+$/;
    return re.test(emai);
}
*/

var usuarioSchema = new Schema({
    genero: String,
    nombre: {
        type: String,
        //trim: true,
        //required: [true, "El nombre es obligatorio"]
    },
    email : {
        type: String,
        //trim: true,
        //required: [true, "El correo electrónico es obligatorio"],
        lowercase: true,
        //unique: true,
        //validate: [validateEmail, "Por favor, ingrese un Email válido"]
        //match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3,4})+$/]
    },
    password:{
        type: String,
        //required: [true, "El password es obligatorio"]
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado:{
        type: Boolean,
        default: false
    }
});

usuarioSchema.plugin(uniqueValidator, { message: 'El {PATH} ya existe con otro usuario'} );

usuarioSchema.methods.reservar = function(biciId, desde, hasta, cb){
    var reserva = new Resererva({ usuario: this._id, bicicleta: biciId, desde: desde, hasta: hasta });
    console.log(reserva);
    reserva.save(cb);
};


usuarioSchema.methods.validPassword = function(user, pass){
    //no soportado en windows
    //return bcrypt.compareSync(password, this.password);

    // usamos el metodo CreateHmac y le pasamos el parametro user y actualizamos el hash con la password
    return crypto.createHmac('sha1', user).update(pass).digest('hex');
}


usuarioSchema.methods.verifyPassword = function(userInfo, password){
    return userInfo.password === crypto.createHmac('sha1', userInfo.nombre).update(password).digest('hex');
}

usuarioSchema.pre('save', function(next){
    if(this.isModified('password'))
        //this.password = bcrypt.hashSync(this.password, saltRounds);
        this.password = crypto.createHmac('sha1', this.nombre).update(this.password).digest('hex');

    next();
});

usuarioSchema.methods.generarToken = function(){

    var valor = "";

    for (var i = 0; i<= 5; i++)
        valor += Math.floor(Math.random() * 10);   //numero entre 0 y 9

    return crypto.createHmac('sha1', valor).digest('hex');
}

usuarioSchema.methods.enviarEmailBienvenida = function(cb){
    
    var token = new Token({ _userId: this.id, token: crypto.randomBytes(16).toString('hex') });
    var destination = this.email;

    token.save(function(err){
        if(err) return err.message;

        var mailOptions = {
            from: "no-repaly@red_bicicletas.com",
            to: destination,
            subject: "Verificación de cuenta",
            text: "Para verificar cuenta haga clik en el siguiente link:  http://localhost:3000/token/confirmacion/" + token.token
         }

        mailer.enviarCorreo(mailOptions);
        console.log('se ha enviado un mensaje de bienvenida a: ' + destination);
    });
}

usuarioSchema.methods.resetPassword = async function(res){
    var token = new Token({ _userId: this.id, token: crypto.randomBytes(16).toString('hex') });
    var destination = this.email;
    console.log(token);

    token.save(function(err){
        if(err) return err.message;

        var mailOptions = {
            from: "no-repaly@red_bicicletas.com",
            to: destination,
            subject: "Reseteo de Password de cuenta",
            text: "Para resetear el password de su cuenta, haga clik en el siguiente link:  http://localhost:3000/resetPassword/token/" + token.token
         }

         //return mailer.enviarCorreo(mailOptions, res);
        
        return mailer.enviarCorreo(mailOptions)
        .then(() => {
            console.log('se ha enviado un token para resetear paswword a: ' + destination);
        }).catch(err => {
            console.log('error al enviar correo: ' + err);
        });

        //return correo;
    });

}

module.exports = mongoose.model('Usuario', usuarioSchema);


//node mailer configuration
/*
const transporter = nodemailer.createTransport({
    host: 'smtp.ethereal.email',
    port: 587,
    auth: {
        user: 'norene66@ethereal.email',
        pass: '7bGwzYTgxK6whuT4Gf'
    }
});
*/




//funciones salt

/*
bcrypt.genSalt(saltRounds, function(err, salt) {
    bcrypt.hash(myPlaintextPassword, salt, function(err, hash) {
        // Store hash in your password DB.
    });
});

bcrypt.hash(myPlaintextPassword, saltRounds, function(err, hash) {
    // Store hash in your password DB.
});


// Load hash from your password DB.
bcrypt.compare(myPlaintextPassword, hash, function(err, res) {
    // res == true
});
bcrypt.compare(someOtherPlaintextPassword, hash, function(err, res) {
    // res == false
});


bcrypt.hash(myPlaintextPassword, saltRounds).then(function(hash) {
    // Store hash in your password DB.
});

// Load hash from your password DB.
bcrypt.compare(myPlaintextPassword, hash).then(function(res) {
    // res == true
});
bcrypt.compare(someOtherPlaintextPassword, hash).then(function(res) {
    // res == false
});


//This is also compatible with async/await

async function checkUser(username, password) {
    //... fetch user from a db etc.
 
    const match = await bcrypt.compare(password, user.passwordHash);
 
    if(match) {
        //login
    }
 
    //...
}


const bcrypt = require('bcrypt');
const saltRounds = 10;
const myPlaintextPassword = 's0/\/\P4$$w0rD';
const someOtherPlaintextPassword = 'not_bacon';

var salt = bcrypt.genSaltSync(saltRounds);
var hash = bcrypt.hashSync(myPlaintextPassword, salt);

var hash = bcrypt.hashSync(myPlaintextPassword, saltRounds);

// Load hash from your password DB.
bcrypt.compareSync(myPlaintextPassword, hash); // true
bcrypt.compareSync(someOtherPlaintextPassword, hash); // false
*/