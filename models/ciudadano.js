var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/test');

/*
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'));
db.once('open', function(){
    console.log("estamos conectados");
});
*/

var ciudadanoSchema = new mongoose.Schema({
    id: Number,
    nombre: String,
    edad: Number
});

var Ciudadano = mongoose.model('Ciudadano', ciudadanoSchema);

var unCiudadano = new Ciudadano({id:1, nombre: "Pedro", edad: 21});
console.log(unCiudadano.nombre);


ciudadanoSchema.methods.saludar = function(){
    var saludo = this.nombre ? "Hola, mi nombre es " + this.nombre : "Hola, no tengo nombre";
    console.log(saludo);
}

unCiudadano.saludar();

unCiudadano.save(function(err, unCiudadano){
    if (err) return console.error(err);
    unCiudadano.saludar(); 
})

Ciudadano.find({nombre: "Pedro"}, callback);


module.exports = Ciudadano;
