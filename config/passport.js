var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy; 
var Usuario = require('../models/usuario');

passport.use(new LocalStrategy( function(email, password, done) {  
    Usuario.findOne({email: email}, function (err, userInfo) { 
        if (err) return done(err); 
        if (!userInfo) return done(null, false, { message: 'Username incorrecto.'});
        if (!userInfo.verifyPassword(userInfo, password)) return done(null, false, { message: 'Password incorrecto.' });
        if (!userInfo.verificado) return done(null, false, { message: 'No se ha verificado el token por correo electrónico.' });
        return done(null, userInfo); 
    }); 
}));

passport.serializeUser(function(user, done) {   
    done(null, user.id); 
}); 
    
passport.deserializeUser(function(id, done) {   
    Usuario.findById(id, function(err, user) {     
        done(err, user);   
    }); 
});

module.exports = passport;