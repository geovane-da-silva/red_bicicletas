var Bicicleta = require('../../models/bicicleta');
var request = require('request');

describe('Bicicleta API', () => {

    var a = new Bicicleta(1, 'roja', 'rural', [14.6170454,-90.5245761]);
    Bicicleta.add(a);

    //get
    describe('GET BICICLETAS /', () => {
        it('Status 200', () => {
            request.get('localhost:3000/api/bicicletas', (req, res) => {
                expect(res.statusCode).toBe(200);
            });
        });
    });

    //post
    describe('POST BICICLETAS /create', () => {
        it('Status 200', () => {
            var headers = {"content-type" : "application-json" };
            var bici = {id: 2, color: "verde", modelo: "rural", ubicacion: [14.5170454,-90.4245761] }
            var url = 'localhost:3000/api/bicicletas/create';

            request.post({
                headers: headers,
                url: url,
                body: bici
            }, 
            function(err, res, body){
                expect(res.statusCode).toBe(200);
                expect(Bicicleta.findById(2).color).toBe("verde");
            });
        });
    });



    //put
    describe('PUT BICICLETAS /update', () => {
        it('Status 200', () => {
            var headers = {"content-type" : "application-json" };
            var bici = Bicicleta.findById(2);
            bici.color = "cafe";
            var url = 'localhost:3000/api/bicicletas/update';

            request.post({
                headers: headers,
                url: url,
                body: bici
            }, 
            function(err, res, body){
                expect(res.statusCode).toBe(200);
                expect(Bicicleta.findById(2).color).toBe("cafe");
            });
        });
    });


    //delete
    describe('DELETE BICICLETAS /delete', () => {
        it('Status 200', () => {
            var headers = {"content-type" : "application-json" };
            var bici = Bicicleta.findById(2);
            var url = 'localhost:3000/api/bicicletas/delete';

            request.post({
                headers: headers,
                url: url,
                body: bici
            }, 
            function(err, res, body){
                expect(res.statusCode).toBe(200);
                expect(Bicicleta.allBicis.length).toBe(1);
            });
        });
    });


});

