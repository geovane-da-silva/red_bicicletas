var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var Usuario = require('../../models/usuario');
var Reserva = require('../../models/reserva');
/*

describe('Testing usuarios', function(){

    beforeEach(function(done){
        var mongodb = 'mongodb://localhost/testdb';
        mongoose.connect(mongodb, {useNewUrlParser: true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'MongoDB connection error:'));
        db.on('open', function(){
            console.log('we are conected to test database');
            done();
        });
    });


    afterEach(function(done){
        Reserva.deleteMany({}, error, succes);
        Usuario.deleteMany({}, error, succes);
        Bicicleta.deleteMany({}, error, succes);
        done();
    });

    describe('cuando un usuario reserva una bici', () => {
        it('Desde existir la reserva', (done) => {
            var usuario = new Usuario({nombre: 'Ezequiel'});
            usuario.save();

            var bicicleta = new Bicicleta({code: 1, color:'roja', modelo: 'urbana', });
            bicicleta.save();

            var hoy = new Date();
            var mañana = new Date();
            mañana.setDate(hoy.setDate() + 1);

            usuario.reservar(bicicleta.id, hoy, mañana, function(error, reserva){
                Reserva.find({}).populate('bicicleta').populate('usuario').exec(function(error, reservas){
                    console.log(reservas[0]);

                    expect(reservas.length).toBe(1);
                    expect(reservas[0].diasReserva()).toBe(2);
                    expect(reservas[0].bicicleta.code).toBe(1);
                    expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
                    done();

                });

            });
        });
    });


});
*/