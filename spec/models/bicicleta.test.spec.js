var mongoose = require('mongoose');
var Bicicleta = require("../../models/bicicleta");

/*
describe('Testing bicicletas', function(){
    beforeEach(function(done){
        if(err) console.log(err);

        var mongoDB = "mongodb://localhost/testdb";
        mongoose.connect(mongoDB, {useNewUrlParser: true});
        var db = mongoose.connection;
        db.on('error', console.error.bind(console, 'MongoDB connection error:'));
        db.once('open', function(){
            console.log("We are conected to test database");
            done();
        });

    });

    afterEach(function(done){
        Bicicleta.deleteMany({},function(err, succes) {
            if(err) console.log(err);
            done();
        });
    });

    
    describe('Bicicleta.createInstance', () => {
        it('crea una instancia de bicicletas', () => {
            var bici = Bicicleta.createInstance(1, 'verde', 'urbana',  [14.6170454,-90.5245761]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe('verde');
            expect(bici.modelo).toBe('urbana');
            expect(bici.ubicacion[0]).toEqueal(14.6170454);
            expect(bici.ubicacion[1]).toEqueal(-90.5245761);

        });
    });

    describe('Bicicleta.allBicis', () => {
        it('comienza vacio', (done) => {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);
                done();
            })
        });
    });

});


//listado
describe('Bicicleta.allBicis', () => {
    it('comienza vacio', function () {
        expect(Bicicleta.allBicis.length).toBe(0);
    })
});



//add
describe('Bicicleta.add', () => {
    it('agregar un registro', function () {

        var kk = Bicicleta.allBicis.length;
        expect(Bicicleta.allBicis.length).toBe(0);

        var a = new Bicicleta(1, 'roja', 'rural', [14.6170454,-90.5245761]);
        Bicicleta.add(a);

        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);

    });
});


//find
describe('Bicicleta.findById', () => {
    it('buscar un registro', function () {

        var bici = Bicicleta.findById(1);
        expect(Bicicleta.allBicis[0]).toBe(bici);
    });
});


//update
describe('Bicicleta.UpdateById', () => {

    it('buscar registro', () => {
        
        expect(Bicicleta.allBicis.length).toBe(1);

        var bici = Bicicleta.findById(1);
        bici.color ="verde";
        bici.modelo = "urbano";

        Bicicleta.UpdateById(bici);

        expect(Bicicleta.allBicis[0].id).toBe(1);
        expect(Bicicleta.allBicis[0].color).toBe('verde');
        expect(Bicicleta.allBicis[0].modelo).toBe('urbano');

    });
});


//delete
describe('Bicicleta.RemoveById', () => {

    it('eliminar registro', () => {
        
        expect(Bicicleta.allBicis.length).toBe(1);
        Bicicleta.RemoveById(1);
        expect(Bicicleta.allBicis.length).toBe(0);

    });

});
*/
