var Usuario = require('../models/usuario');

module.exports = {

    list: function(req, res, next){
        Usuario.find({}, function(err, usuarios) {
            if(err) return res.status(500).send(err);
            res.render('usuarios/index', { usuarios: usuarios});
        });
    },

    update_get: function(req, res, next){
        Usuario.findById(req.params._id, function(err, usuario, next){
            if(err) return res.status(500).send(err);
            res.render('usuarios/update', { errors: {}, usuario: usuario});
        });
    },

    update_post: function(req, res, next){
        var update_values = {nombre: req.body.nombre};
        Usuario.findByIdAndUpdate(req.params.id, update_values, function(err, usuario){
            if(err) return res.render('usuarios/upadate', {errors: err.errors, usuario: new Usuario()});
            return res.redirect('usuarios');
        });
    },

    create_get: function(req, res, next){
        res.render('usuarios/create', { errors: {}, usuario: new Usuario()});
    },

    create_post: function(req, res, next){
        var nuevo = { nombre: req.body.nombre, email: req.body.email,  password: req.body.password, confirm_password: req.body.confirm_password };
        Usuario.create(nuevo, function(err, usuario){
            //if(err) return res.status(500).send(err);
            if(err) return res.render('usuarios/create', { errors: err.errors, usuario: new Usuario() });

            usuario.enviarEmailBienvenida();
            return res.redirect('/usuarios');
        });
        
    },

    delete_post: function(req, res, next){
        Usuario.findByIdAndDelete(req.params.id, function(err){
            if(err) return next(err);
            return res.redirect('/usuarios');
        });
    }

}

/*
exports.list = function(req, res){
    res.render('usuarios/index', {bicis: Bicicleta.allBicis});
};

exports.create_get = function(req, res){
    res.render('usuarios/create');
}

exports.create_post = function(req, res){

    var bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lat, req.body.lng];
    Bicicleta.add(bici);

    res.redirect('/usuarios');
}

exports.update_get = function(req, res){
    var bici = Bicicleta.findById(req.params.id);
    res.render('usuarios/update', {bici});
}

exports.update_post = function(req, res){
    var bici = Bicicleta.findById(req.params.id);

    bici.id = req.body.id;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.lng]; 
    
    res.redirect('/usuarios');
}

exports.delete_post = function(req, res){
    Bicicleta.RemoveById(req.body.id);
    res.redirect('/usuarios');
}
*/