var Bicicleta = require('../../models/bicicleta');
var mailer = require('../../mailer/mail');
//const nodemailer = require("nodemailer");


exports.Bicicleta_List = function(req, res){

    /*
    var crypto = require('crypto');
    
    var data = {
        user: "test",
        pass: "admin",
        crypto: crypto.createHmac('sha1', "test").update("admin").digest('hex'),
        crypto2: crypto.createHmac('sha1', "admin").digest('hex'),
        crypto3: crypto.createHmac('sha1', "test").update("admin").digest('hex'),
        crypto3: crypto.createHmac('sha1', "test2").update("admin").digest('hex'),
        crypto4: crypto.createHmac('sha1', "test").update("admin").digest('hex'),
    }

    return res.status(200).send(data);
    */



    /*
    var info = {
        from: '"Fred Foo ?" <wallychapin3@gmail.com>', // sender address
        to: 'wiltonmonroy@gmail.com, wallychapin@gmail.com', // list of receivers
        subject: 'Hello ✔', // Subject line
        text: 'Hello world 2222?', // plaintext body
        html: '<b>Hello world desde node.js?</b>' // html body
    }

    mailer.enviarCorreo(info, res);
    */

    Bicicleta.allBicis(req, res);
}

exports.Bicicleta_Create = function(req, res){

    //console.log(req.body.color);
    //console.log(req.body.id);
    
    /*
    var bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
    console.log(req.body.color);
    bici.ubicacion = [req.body.lat, req.body.lng];

    res.status(200).json({
        bicicleta: bici
    });
    */

    var bici = {code: req.body.id, modelo: req.body.modelo, color: req.body.color, ubicacion: [req.body.lat, req.body.lng] }
    Bicicleta.add(bici, res);
}

exports.Bicicleta_Delete = function(req, res){
    /* 
    Bicicleta.RemoveById(req.body.id)
    res.status(200).send();
    */

    Bicicleta.RemoveById(req.body.id, res);
}

exports.Bicicleta_Update = function(req, res){
    var bici = {_id: req.body.id, code: req.body.code, modelo: req.body.modelo, color: req.body.color, ubicacion: [req.body.lat, req.body.lng] }
    Bicicleta.UpdateById(bici, res);

   //var bici = {code: req.body.id, modelo: req.body.modelo, color: req.body.color, ubicacion: [req.body.lat, req.body.lng] }

    //res.status(200).send(bici);

    //Bicicleta.UpdateById(bici, res);
    //res.status(200).send();
    
}


exports.Bicicleta_FindById = function(req, res){

    Bicicleta.findById(req.params.id, res);

    /*
    var bici = Bicicleta.findById(req.body.id);

    if(bici)
        res.status(200),send(bici);
    else
        res.status(200).send({error: "No existe la bicicleta con id: " + req.body.id});
    */

    //Bicicleta.RemoveById(req.body.id, res);
}