var Usuario = require('../../models/usuario');

exports.usuarios_list = function(req, res){
    Usuario.find({}, function(err, data){
        if(err) res.status(500).send(err);
        res.status(200).send(data);
    });
}

exports.usuario_create = function(req, res){
    var usuario = new Usuario({nombre: req.body.nombre, email: req.body.email, password: req.body.password, verificado: false, genero: req.body.genero});
    usuario.save(function(err){
        if(err) return res.status(500).send(err);
        usuario.enviarEmailBienvenida();
        return res.status(200).send(usuario);
    });
}

exports.usuario_reservar = function(req, res){
    Usuario.findById(req.body.id, function(err, reserva){
        if(err) res.status(500).send(err);
        console.log('reserva!!!');
        res.status(200).send(reserva);
    });
}