var Usuario = require('../../models/usuario');
var crypto = require('crypto');
var jwt = require('jsonwebtoken');

module.exports = {
    authenticate: function(req, res, next){
        Usuario.findOne({email: req.body.email}, function(err, userInfo){
            if(err) return next(err);
            if(userInfo === null ) return res.status(401).json({ status: "error", message: "Inválido email/password", data: null });
            
            if(userInfo !== null && userInfo.verifyPassword(userInfo, req.body.password)){
            //if(userInfo !== null && crypto.createHmac('sha1', userInfo.nombre).update(req.body.password).digest('hex') === userInfo.password){
                var token = jwt.sign({ id: userInfo._id }, req.app.get('secretKey'), { expiresIn: '7d'} );
                return res.status(200).json({ message: "usuario encontrado", data: {usuario: userInfo, token: token} });
            }
            else{
                return res.status(401).json({ status: "error", message: "Inválido email/password", data: null });
            }
        });
    },
    forgotPassword: async function(req, res, next){
        Usuario.findOne({ email: req.body.email }, function(err, usuario){
            if(err) return res.status(500).send(err);
            if(!usuario) return res.status(401).json({ message: "No existe el usuario", data: null});

            usuario.resetPassword().then(() => {
                return res.status(200).json({ message: "Se envió un email para restablecer el password", data: null });
            })
            .catch(err => {
                return res.status(500).send(err);
            });

            /*
            usuario.resetPassword(function(err){
                if(err) return res.status(500).send(err);
                return res.status(200).send("correo enviado");
            });
            */

        });
    }
}