var Usuario = require('../models/usuario');
var Token = require('../models/token');

module.exports = {
    confirmationGet: function(req, res, next){
        
        Token.findOne( { token: req.params.token }, function(err, token){
            if(err) return res.status(500).send(err);
            if(!token) return res.status(400).send( { type: "not-veryfied", msg: "No se encontró un token con ese valor" });
            
            Usuario.findById(token._userId, function(err, usuario){
                if(err) return res.status(500).send(err);
                if(!usuario) return res.status(400).send( { msg: "No se encontró un usuario con ese valor"});
                if(usuario.verificado) return res.render('/usuarios');

                usuario.verificado = true;
                usuario.save(function(err){
                    if(err) return res.status(500).send( { msg: err.message });
                    res.redirect('/login');
                });
            });
        });
    }
}