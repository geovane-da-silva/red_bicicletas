var express = require('express');
var router = express.Router();
var controller = require('../controllers/bicicleta');

router.get('/', controller.Bicicleta_List);
router.get('/create', controller.Bicicleta_Create_get);
router.post('/create', controller.Bicicleta_Create_post);
router.get('/:id/update', controller.Bicicleta_Update_get);
router.post('/:id/update', controller.Bicicleta_Update_post);
router.post('/:id/delete', controller.Bicicleta_Delete_post);

module.exports = router;