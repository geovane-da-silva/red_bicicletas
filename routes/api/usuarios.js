var express = require('express');
var router = express.Router();
var controller = require('../../controllers/api/usuario');

router.get('/', controller.usuarios_list);
router.post('/create', controller.usuario_create);
router.post('/reservar', controller.usuario_reservar);

module.exports = router;