var express = require('express');
var router = express.Router();
var controller = require('../../controllers/api/auth');

router.post('/authenticate', controller.authenticate);
router.post('/forgotPassword', controller.forgotPassword);

module.exports = router;