var express = require('express');
var router = express.Router();
var apiController = require('../../controllers/api/bicicletas');

router.get('/', apiController.Bicicleta_List);
router.get('/:id', apiController.Bicicleta_FindById);
router.post('/create', apiController.Bicicleta_Create);
router.post('/delete', apiController.Bicicleta_Delete);
router.post('/update', apiController.Bicicleta_Update);

module.exports = router;
